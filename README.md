# Notebook tutorials

Here you'll find a selection of notebook tutorials which have featured in one of LTU Codeup's workshops.

The tutorials currently include:

+ [Tweepy tutorial](tweepy/tweepy_tutorial.ipynb): an intro to mining data from Twitter
+ [OCR tutorial](ocr/ocr_tutorial.ipynb): an intro to text mining from images
